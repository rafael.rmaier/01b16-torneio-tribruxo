
import './App.css';
import { useEffect, useState } from 'react';
import {Students} from './components/Students';
import {WelcomePage} from './components/WelcomePage';

function App() {
  const [studentArray, setStudentArray] = useState([]);

  const [canLoadStudents, setCanLoadStudents] = useState(false);

  useEffect(() => {
    fetch("https://hp-api.herokuapp.com/api/characters/students")
    .then((results) => results.json())
    .then((results) => setStudentArray(results));
     

  }, [studentArray])


  return (
    <div className="App">
      <div className="App-header">
 
          

        {canLoadStudents ? (
            <Students studentArray={studentArray} setCanLoadStudents={setCanLoadStudents}/>
        ):(
          
          <WelcomePage setCanLoadStudents={setCanLoadStudents}/>
        )

        }
        
      
        
      </div>
    </div>
  );
}

export default App;
