import { useEffect, useState } from "react";
import "./styles.css";


export const Students = ({studentArray, setCanLoadStudents}) => {
    const [count, setCount] = useState(0);
    const [random, setRandom] = useState(Math.floor(Math.random() * 11));
    const [studentsForDisplay, setStudentsForDisplay] = useState([studentArray[random]]);

 

    useEffect(() => {

        if(count < 2){
        selectRandomStudent();
        }
    })


    const selectRandomStudent = () => {
        setRandom(Math.floor(Math.random() * 10));

        if((studentsForDisplay.every((el) => el.name !== studentArray[random].name)) && 
        (studentsForDisplay.every((el) => el.house !== studentArray[random].house))){
            setStudentsForDisplay([...studentsForDisplay, studentArray[random]]);
            setCount(count + 1);
           
        }


    }
    
 
    


    return(
        <div className="wizAndButton">
        <div className="wizContainer">
        {studentsForDisplay.map((el, index) => (
            <div className={el.house} key={index}>
                <figure>
                 <img className="wizpic" alt="wizard" src={el.image}></img>
                </figure>
            <p className="wizardName">{el.name}</p>
            <p className={el.house}>{el.house}</p>
            
            </div>
        ))} 
       
        </div>
        <button onClick={() => setCanLoadStudents(false)}>Tentar novamente</button>
        </div>
    )

}