import logo from '../../logo.svg';
import sortinghat from '../../images/sortinghat_adobespark.png'
import glasses from '../../images/harrypotterglasses_adobespark.png';
import './styles.css'
export const WelcomePage = ({setCanLoadStudents}) => {
    
    return(
        <div className="welcome">
            <div className="reactHat">
            <img src={sortinghat} alt ="sorting hat"className="sortinghat"></img>
            {/* <img src={glasses} alt="harry potter glasses" className="glasses"></img> */}
            <img src={logo} alt="react logo" className="logo"></img>
            </div>
            <h1>Torneio Tribruxo</h1>
            <p>Clique no botão para encontrar os feiticeros!</p>
            <button onClick={() => setCanLoadStudents(true)}>Começar!</button>

        </div>
    )
}
    